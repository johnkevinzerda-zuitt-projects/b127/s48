import {Fragment, useEffect, useState, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
//React Context
import UserContext from '../UserContext'

export default function Login () {
	//useContext is a react hook used to unwrap our context. It will return the data passed as values by a provider(UserContext.Provider component in App.js)
	const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password,setPassword] =useState('');
	const [isActive,setIsActive] = useState(false);

	useEffect(() => {
		if (email!==''&& password!=='') {
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	},[email,password]);

	let loginUser = e => {
		e.preventDefault();
		setEmail('');
		setPassword('');

		Swal.fire(
		{
			title: 'Login successful!',
			icon: 'success',
			text: 'Please wait'
		}
		)
		//local storage allows us to save data within our browses as strings\
		//The setItem() method of the Storage object, or update the key's value if it already exists
		//setItem is used to store data in the localStorage as a string
		//setItem('key',value)
		localStorage.setItem('email', email);
		setUser({emal: email});
	}

	return(
		<Fragment>
		<Form onSubmit ={e=>loginUser(e)}>
		<h1>Login</h1>
		<Form.Group>
		<Form.Label>Email address:</Form.Label>
		<Form.Control 
		type="email"
		placeholder="Enter email"
		value={email}
		onChange= {e=>setEmail(e.target.value)}
		required
		/>
		<Form.Label>Password:</Form.Label>
		<Form.Control 
		type="password"
		placeholder="Enter password"
		value={password}
		onChange= {e=>setPassword(e.target.value)}
		required
		/>
		</Form.Group>
		{isActive ?
			<Button type="submit" variant="success" id="loginBtn">Login</Button>
			:
			<Button type="submit" variant="success" id="loginBtn" disabled>Login</Button>
		}

		</Form>
		</Fragment>
	)


}